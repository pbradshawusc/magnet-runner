//
//  Background.h
//  Magnet Runner
//
//  Created by Joseph Lin on 2/21/14.
//  Copyright (c) 2014 ITP382. All rights reserved.
//

#import "CCLayer.h"
#import "cocos2d.h"

@interface Background : CCLayer
{
    CCSprite * bground;
    CCSprite * next;
}
-(void) move: (int) speed;
@end
