//
//  GameplayScene.m
//  Magnet Runner
//
//  Created by Alexa Rucks on 2/16/14.
//  Copyright (c) 2014 ITP382. All rights reserved.
//

#import "TutorialScene.h"
#import "ObstacleFactory.h"
#import "MainMenuScene.h"
#import "GameOverScene.h"
#import "Shrapnel.h"
#import "DestructibleObstacle.h"
#import "SimpleAudioEngine.h"

@implementation TutorialScene
-(id) init
{
    self = [super initNew];
        [[SimpleAudioEngine sharedEngine] playBackgroundMusic:@"Full Force TOS.mp3" loop:YES];
        
        //Read in the current Game Data and initialize where needed
        gameData = [GameData GetGameData];
        gameData.time = 0;
        gameData.score = 0;
        gameData.speed = 4;
        [gameData SetGameplayScene:self];
        
        //Read in/Create the player data
        player = [Player GetPlayer];
        
        shrapnel = [[NSMutableArray alloc] init];
        
        //Adding a Background
        backgroundlayer = [Background node];
        
        //Displays score
        CGSize s = [CCDirector sharedDirector].winSize;
        
        //Displays Instructions
        label = [CCLabelTTF labelWithString:@"Welcome to Magnet Runner!" fontName:@"Marker Felt" fontSize:24];
		[self addChild:label z:1];
		[label setColor:ccc3(255,255,255)];
		label.position = ccp( s.width/2, s.height-(label.contentSize.height/2));
        state = Intro;
        timePassed = 0;
        objectiveCompleted = true;
    destroyed = false;
    
        //Create and add the sprite layer where the Box2D world is emulated
        spriteLayer = [SpriteLayer node];
        [self addChild:spriteLayer z:0];
        
        //Initialize the obstacle array
        obstacles = nil;
        obstacleTime = 0;
        playedSound = false;
        [self addChild:backgroundlayer z:-1];
        
        levelNum = 0;
        
        //Schedule the scene for updates to update all layers within
    //[self update:1];
    [self scheduleUpdate];
    
    return self;
}
-(void) update:(ccTime)dt
{
    timePassed += dt;
    //NSLog(@"%f",timePassed);
    CGSize s = [CCDirector sharedDirector].winSize;
    
    if(state == Intro)
    {
        if(timePassed > 3)
        {
            NSLog(@"Change Gravity");
            objectiveCompleted = false;
            timePassed = 0;
            state = ChangeGravity;
            
            [label removeFromParentAndCleanup:YES];
            label = [CCLabelTTF labelWithString:@"Tap the Screen to Reverse Gravity" fontName:@"Marker Felt" fontSize:24];
            [self addChild:label z:1];
            [label setColor:ccc3(255,255,255)];
            label.position = ccp( s.width/2, s.height-(label.contentSize.height/2));
        }
    }
    else if(state == ChangeGravity)
    {
        if(timePassed > 3 && objectiveCompleted)
        {
            NSLog(@"Avoid Obstacle");
            objectiveCompleted = false;
            timePassed = 0;
            state = AvoidObstacle;
            
            Obstacle *obst = [[Obstacle alloc] initWithWidth:40 Height:(s.height/2) X:0 Top:true];
            [spriteLayer addChild:[obst getSprite]];
            [obstacles addObject:obst];
            
            [label removeFromParentAndCleanup:YES];
            label = [CCLabelTTF labelWithString:@"Reverse Gravity to Avoid" fontName:@"Marker Felt" fontSize:24];
            [self addChild:label z:1];
            [label setColor:ccc3(255,255,255)];
            label.position = ccp( s.width/2, s.height-(label.contentSize.height/2));
            
            [self pauseSchedulerAndActions];
        }
    }
    else if(state == AvoidObstacle)
    {
        if(timePassed > 7 && objectiveCompleted)
        {
            objectiveCompleted = true;
            timePassed = 0;
            state = HitShrapnel;
            
            Shrapnel *sh = [[Shrapnel alloc] initAtBottom];
            [spriteLayer addChild:[sh GetSprite]];
            [shrapnel addObject:sh];
            
            [label removeFromParentAndCleanup:YES];
            label = [CCLabelTTF labelWithString:@"Shrapnel Increases Gravitational Pull" fontName:@"Marker Felt" fontSize:24];
            [self addChild:label z:1];
            [label setColor:ccc3(255,255,255)];
            label.position = ccp( s.width/2, s.height-(label.contentSize.height/2));
        }
    }
    else if(state == HitShrapnel)
    {
        if(timePassed > 7 && objectiveCompleted)
        {
            objectiveCompleted = false;
            timePassed = 0;
            state = DestroyWall;
            
            DestructibleObstacle *dest = [[DestructibleObstacle alloc] init];
            [spriteLayer setDest:dest];
            [spriteLayer addChild:[dest getSprite]];
            [obstacles addObject:dest];
            [[SimpleAudioEngine sharedEngine] playEffect:@"Alert SE.mp3"];
            
            [label removeFromParentAndCleanup:YES];
            label = [CCLabelTTF labelWithString:@"Keep Tapping the Wall!" fontName:@"Marker Felt" fontSize:24];
            [self addChild:label z:1];
            [label setColor:ccc3(255,255,255)];
            label.position = ccp( s.width/2, s.height-(label.contentSize.height/2));
        }
    }
    else if(state == DestroyWall)
    {
        if(timePassed > 3 && !destroyed)
        {
            if(![[self scheduler] isPaused])
            {
                [self pauseSchedulerAndActions];
            }
        }
        if(timePassed > 5 && objectiveCompleted)
        {
            objectiveCompleted = false;
            timePassed = 0;
            state = Done;
            
            [label removeFromParentAndCleanup:YES];
            label = [CCLabelTTF labelWithString:@"That's It! Enjoy Playing!" fontName:@"Marker Felt" fontSize:24];
            [self addChild:label z:1];
            [label setColor:ccc3(255,255,255)];
            label.position = ccp( s.width/2, s.height-(label.contentSize.height/2));
        }
    }
    else if(state == Done)
    {
        if(timePassed > 5)
        {
            [[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
            [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:0.5 scene:[MainMenuScene node]]];
        }
    }
    
    [spriteLayer update:dt];
    for(NSInteger i = [shrapnel count]-1; i >= 0; i--)
    {
        Shrapnel *s = [shrapnel objectAtIndex:i];
        if([s hasCollided])
        {
            [s destroy];
            [shrapnel removeObjectAtIndex:i];
        }
    }
    [spriteLayer checkDest];
    gameData.time += dt;
    gameData.distance += gameData.speed*dt;
    gameData.score = (gameData.distance*gameData.distance/gameData.time)*(player.numShrapnelHit+1);
    [player update:dt];
    [backgroundlayer move:[gameData speed]];
    if(obstacles)
    {
        for(NSInteger i = [obstacles count]-1; i >= 0; i--)
        {
            Obstacle *o = [obstacles objectAtIndex:i];
            if([o checkBounds])
            {
                [o destroy];
                [obstacles removeObjectAtIndex:i];
            }
        }
    }
}
-(void) ObjectiveCompleted
{
    objectiveCompleted = true;
    NSLog(@"Objective Completed");
    [self resumeSchedulerAndActions];
}
-(void) destroyedWall
{
    destroyed = true;
}
-(bool) GetCompleted
{
    return objectiveCompleted;
}
-(void) LoseConditionTriggered
{
    [self unscheduleUpdate];
    [[SimpleAudioEngine sharedEngine] playEffect:@"Game Over SE.mp3"];
    [[SimpleAudioEngine sharedEngine] playBackgroundMusic:@"The Grudge TOS.mp3" loop:YES];
    [gameData check_score];
    [player clean];
    //[gameData clean];
    [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:1.0 scene:[GameOverScene node] ]];
}

@end
