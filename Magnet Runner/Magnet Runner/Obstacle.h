//
//  Obstacle.h
//  Magnet Runner
//
//  Created by Alexa Rucks on 2/19/14.
//  Copyright (c) 2014 ITP382. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "Box2D.h"
#import "CCPhysicsSprite.h"
#import "GameData.h"

@interface Obstacle : NSObject
{
    int width, height;
    b2BodyDef bodyDef;
    b2Body *body;
    b2PolygonShape dynamicBox;
    b2FixtureDef fixtureDef;
    CCPhysicsSprite *sprite;
    GameData *data;
    b2Joint *joint;
    bool destroyed;
}
-(Obstacle *) initWithWidth:(int)w Height:(int)h X:(int)x Top:(bool)t;
-(CCPhysicsSprite *) getSprite;
-(NSString *) toString;
-(bool) checkBounds;
-(b2Body *) GetBody;
-(NSString *) GetType;
-(void) destroy;
@end
