//
//  OptionsMenu.h
//  Magnet Runner
//
//  Created by Alexa Rucks on 3/8/14.
//  Copyright (c) 2014 ITP382. All rights reserved.
//

#import "CCScene.h"

@interface OptionsMenu : CCScene
{
    UITextField* textField;
}
+(CCScene *) scene;
@end
