//
//  GameplayScene.h
//  Magnet Runner
//
//  Created by Alexa Rucks on 2/16/14.
//  Copyright (c) 2014 ITP382. All rights reserved.
//

#import "CCScene.h"
#import "SpriteLayer.h"
#import "GameData.h"
#import "Player.h"
#import "Obstacle.h"
#import "Background.h"
#import "GameplayScene.h"
@interface TutorialScene : GameplayScene
{
    /*SpriteLayer *spriteLayer;
    GameData *gameData;
    Player *player;
    CCLabelTTF * scorelabel;
    CCLabelTTF *label;
    NSMutableArray *obstacles;
    Background *backgroundlayer;
    NSMutableArray *shrapnel;
    
    int obstacleTime;
    int levelNum;
    bool playedSound;*/
    enum TutorialState {
        Intro, ChangeGravity, AvoidObstacle, HitShrapnel, DestroyWall, Done
    };
    enum TutorialState state;
    float timePassed;
    bool objectiveCompleted;
    bool destroyed;
}
-(void) LoseConditionTriggered;
-(void) ObjectiveCompleted;
-(bool) GetCompleted;
-(void) update:(ccTime)dt;
-(void) destroyedWall;
@end
