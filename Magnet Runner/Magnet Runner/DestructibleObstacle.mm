//
//  DestructibleObstacle.m
//  Magnet Runner
//
//  Created by Alexa Rucks on 3/8/14.
//  Copyright (c) 2014 ITP382. All rights reserved.
//

#import "DestructibleObstacle.h"

@implementation DestructibleObstacle
-(id) init
{
    timesTapped = 0;
    CGSize size = [[CCDirector sharedDirector] winSize];
    self = [self initWithWidth:40 Height:size.height X:300 Top:false];
    [sprite initWithFile:@"breakablewall.png"];
    [sprite setScaleX:width/sprite.contentSize.width];
    [sprite setScaleY:height/sprite.contentSize.height];
    return self;
}
-(void) touched
{
    timesTapped++;
    NSLog(@"%d", timesTapped);
    if(timesTapped > 3)
    {
        [super destroy];
        [[[GameData GetGameData] GetGameplayScene] ObjectiveCompleted];
    }
}
-(void) destroy
{
    [super destroy];
}
-(bool) getDestroyed
{
    return destroyed;
}
- (CGRect) rect
{
    return CGRectMake((self->body->GetPosition().x*PTM_RATIO)-(self->width/2), (self->body->GetPosition().y*PTM_RATIO)-(self->height/2), self->width, self->height);
}
-(void) dealloc
{
    [super dealloc];
}
@end
