//
//  DestructibleObstacle.h
//  Magnet Runner
//
//  Created by Alexa Rucks on 3/8/14.
//  Copyright (c) 2014 ITP382. All rights reserved.
//

#import "Obstacle.h"

@interface DestructibleObstacle : Obstacle
{
    int timesTapped;
}
-(void) touched;
-(CGRect) rect;
-(bool) getDestroyed;
-(void) destroy;
@end
