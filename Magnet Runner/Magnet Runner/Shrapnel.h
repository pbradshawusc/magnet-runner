//
//  Shrapnel.h
//  Magnet Runner
//
//  Created by James Lynch on 2/19/14.
//  Copyright (c) 2014 ITP382. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GameData.h"
#import "CCPhysicsSprite.h"

#define PTM_RATIO 32

@interface Shrapnel : NSObject {
    GameData * data;
    
    b2BodyDef bodyDef;
    b2Body * body;
    b2PolygonShape bodyShape;
    b2FixtureDef fixtureDef;
    CCPhysicsSprite *sprite;
    
    int spriteindex;
    int attractiveness;
    bool collided;
    bool destroyed;
}

@property int shrapnelAttractiveness;

+ (Shrapnel *) getShrapnelwithAttractiveness: (int) attractiveness;
-(id) initAtBottom;
- (void) update: (ccTime) dt;
- (void) hitPlayer;
-(NSString *) GetType;
-(CCPhysicsSprite *) GetSprite;
-(void) destroy;
-(bool) hasCollided;
-(CGRect) rect;
-(void) setAnimation;
-(void) touched;
@end
