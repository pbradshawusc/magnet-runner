//
//  customCollisionListener.m
//  Magnet Runner
//
//  Created by Alexa Rucks on 2/21/14.
//  Copyright (c) 2014 ITP382. All rights reserved.
//

#import "customCollisionListener.h"
#import "Player.h"
#import "Obstacle.h"
#import "SimpleAudioEngine.h"

customCollisionListener::customCollisionListener() {
}

customCollisionListener::~customCollisionListener() {
}

void customCollisionListener::setGameplayScene(GameplayScene *gp)
{
    gpScene = gp;
}

void customCollisionListener::BeginContact(b2Contact* contact) {
    // We need to copy out the data because the b2Contact passed in
    // is reused.
    //MyContact myContact = { contact->GetFixtureA(), contact->GetFixtureB() };
    //_contacts.push_back(myContact);
    
    b2ContactListener::BeginContact(contact);
    
    b2WorldManifold worldManifold; //Holds all the contact points of contact.
    contact->GetWorldManifold(&worldManifold); // this method calls b2WorldManifold::Initialize with the appropriate transforms and radii so you don't have to worry about that
    b2Vec2 worldNormal = worldManifold.normal; // this points from A to B

    CGSize size = [[CCDirector sharedDirector] winSize];
    NSObject *p = (NSObject *)(contact->GetFixtureA()->GetUserData());
    NSObject *o = (NSObject *)(contact->GetFixtureB()->GetUserData());
    NSString *pType = [p GetType];
    NSString *oType = [o GetType];
    b2AABB pAABB = contact->GetFixtureA()->GetAABB(0); //b2AABB = "axis aligned bounding box", or the box surrounding the fixture
    b2AABB oAABB = contact->GetFixtureB()->GetAABB(0);
    if([oType isEqualToString:@"player"]) //This just swaps o and p so that the player is always in the pType.
    {
        NSObject *temp = o;
        b2AABB tAABB = oAABB;
        o = p;
        p = temp;
        oType = pType;
        pType = @"player";
        oAABB = pAABB;
        pAABB = tAABB;
        worldNormal.y = -worldNormal.y;
    }
    float oWidth = oAABB.upperBound.x - oAABB.lowerBound.x;
    float oHeight = oAABB.upperBound.y - oAABB.lowerBound.y;
    float pWidth = pAABB.upperBound.x - pAABB.upperBound.y;
    float pHeight = pAABB.upperBound.y - pAABB.lowerBound.y;
    if([pType isEqualToString:@"player"]){
        if([oType isEqualToString:@"obstacle"])
        {
            //NSLog(@"World Normal: %f \nCollision Normal: %f", worldNormal.y, -oWidth/sqrt(oWidth*oWidth+oHeight*oHeight));
            [[SimpleAudioEngine sharedEngine] playEffect:@"Jab SE.wav"];
            if((oAABB.lowerBound.x)<(pAABB.lowerBound.x))
            {
                return;
            }
            if(oAABB.lowerBound.y < 1 && worldNormal.y >= -oWidth/sqrt(oWidth*oWidth+oHeight*oHeight))
            {
                [gpScene LoseConditionTriggered];
            }
            else if(oAABB.upperBound.y > (size.height/PTM_RATIO) - 1 && worldNormal.y <= oWidth/sqrt(oWidth*oWidth+oHeight*oHeight))
            {
                [gpScene LoseConditionTriggered];
            }
        }
        else if([oType isEqualToString:@"shrapnel"])
        {
            [[[GameData GetGameData] GetGameplayScene] ObjectiveCompleted];
            [[SimpleAudioEngine sharedEngine] playEffect:@"Jab SE.wav"];
            [p hitShrapnel];
            [o hitPlayer];
            return;
        }
    }
    
}

void customCollisionListener::EndContact(b2Contact* contact) {
    b2ContactListener::EndContact(contact);
}

void customCollisionListener::PreSolve(b2Contact* contact,
                                 const b2Manifold* oldManifold) {
    b2ContactListener::PreSolve(contact, oldManifold);
}

void customCollisionListener::PostSolve(b2Contact* contact,
                                  const b2ContactImpulse* impulse) {
    b2ContactListener::PostSolve(contact, impulse);
}
