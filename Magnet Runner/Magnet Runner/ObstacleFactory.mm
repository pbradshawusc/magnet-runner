//
//  ObstacleFactory.m
//  Magnet Runner
//
//  Created by Alexa Rucks on 2/20/14.
//  Copyright (c) 2014 ITP382. All rights reserved.
//

#import "ObstacleFactory.h"
#import "Obstacle.h"
#import "Player.h"

static GameData *data = nil;

@implementation ObstacleFactory
+(NSMutableArray *) GenerateLevel:(int)l
{
    //if data is nil, initialize it
    if(data == nil)
    {
        data = [GameData GetGameData];
    }
    
    //Initialize the obstacle array
    NSMutableArray *obstacles;
    
    if(l == 1)
    {
        obstacles = [NSMutableArray arrayWithObject:[[Obstacle alloc] initWithWidth:40 Height:80 X:0 Top:false]];
        [obstacles addObject:[[Obstacle alloc] initWithWidth:40 Height:80 X:0 Top:true]];
        [obstacles addObject:[[Obstacle alloc] initWithWidth:40 Height:120 X:160 Top:false]];
        [obstacles addObject:[[Obstacle alloc] initWithWidth:40 Height:30 X:400 Top:false]];
        [obstacles addObject:[[Obstacle alloc] initWithWidth:40 Height:100 X:400 Top:true]];
    }
    else
    {
        obstacles = [NSMutableArray arrayWithObject:[[Obstacle alloc] initWithWidth:40 Height:40 X:0 Top:false]];
        [obstacles addObject:[[Obstacle alloc] initWithWidth:40 Height:40 X:0 Top:true]];
        [obstacles addObject:[[Obstacle alloc] initWithWidth:40 Height:170 X:40 Top:false]];
        [obstacles addObject:[[Obstacle alloc] initWithWidth:20 Height:20 X:240 Top:false]];
        [obstacles addObject:[[Obstacle alloc] initWithWidth:20 Height:200 X:240 Top:true]];
    }
    
    return obstacles;
}
+(NSMutableArray *) ProcedurallyGenerateLevel
{
    //if data is nil, initialize it
    if(data == nil)
    {
        data = [GameData GetGameData];
    }
    
    int buffer = 720/(10+data.speed);
    
    CGSize s = [CCDirector sharedDirector].winSize;
    NSMutableArray *obstacles = [NSMutableArray arrayWithObject:[[Obstacle alloc] initWithWidth:40 Height:((s.height/2)-buffer) X:0 Top:false]];
    [obstacles addObject:[[Obstacle alloc] initWithWidth:40 Height:((s.height/2)-buffer) X:0 Top:true]];
    CGPoint originalPosition(ccp(s.height/2, s.height/2));
    float currentDist = 0;
    float distance;
    float time;
    
    //Find Path First, then make obstacles to fit the path
    //int i = 0;
    //for(int i = 1; i < 6; i++)
    //Defines the next level. If ending, provides a barrier to end.
    while(currentDist < 1000)
    {
        //i++;
        distance = 160+arc4random_uniform(130);
        time = (distance/data.speed);
        currentDist+=distance;
        int yMax = originalPosition.y + 5*time*time; //p = x + vt + (1/2)at^2
        int yMin = originalPosition.y - 5*time*time;
        int yNext = -1;
        do
        {
            yNext = arc4random_uniform(yMax+yMin);
        }
        while(yNext < yMin || yNext < buffer || yNext > yMax || yNext > (s.height-buffer));
        if(yNext-buffer > 0 && arc4random_uniform(30)>10)
        {
            [obstacles addObject:[[Obstacle alloc] initWithWidth:40 Height:(yNext-buffer) X:currentDist Top:false]];
        }
        if(yNext + buffer < s.height && arc4random_uniform(30)>10)
        {
            [obstacles addObject:[[Obstacle alloc] initWithWidth:40 Height:(s.height - (yNext+buffer)) X:currentDist Top:true]];
        }
        
        originalPosition.y = yNext;
    }
    
    /*int firstHeight = arc4random() % (int) s.height;
    while (firstHeight > s.height*.65) {
        firstHeight = arc4random() % (int) s.height;
    }
    int secondHeight = arc4random() % (int) s.height;
    while (secondHeight >= (s.height - firstHeight) + (s.height*.2)){
        secondHeight = arc4random() % (int) s.height;
    }
    int distance = arc4random() % 60;
    
    //Initialize the obstacle array
    NSMutableArray *obstacles = [NSMutableArray arrayWithObject:[[Obstacle alloc] initWithWidth:40 Height:firstHeight X:(30 + distance) Top:false]];
    [obstacles addObject:[[Obstacle alloc] initWithWidth:40 Height:secondHeight X:distance Top:true]];*/

    return obstacles;
}
@end
