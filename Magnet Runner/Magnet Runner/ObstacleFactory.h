//
//  ObstacleFactory.h
//  Magnet Runner
//
//  Created by Alexa Rucks on 2/20/14.
//  Copyright (c) 2014 ITP382. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ObstacleFactory : NSObject
+(NSMutableArray *) GenerateLevel:(int)l;
+(NSMutableArray *) ProcedurallyGenerateLevel;
@end
