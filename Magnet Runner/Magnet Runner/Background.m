//
//  Background.m
//  Magnet Runner
//
//  Created by Joseph Lin on 2/21/14.
//  Copyright (c) 2014 ITP382. All rights reserved.
//

#import "Background.h"

@implementation Background
- (id)init
{
    self = [super init];
    if (self) {
        CGSize size = [CCDirector sharedDirector].winSize;
		NSLog(@"bground made");
		
        bground = [CCSprite spriteWithFile:@"background1.png"];
        bground.rotation = 0;
        bground.position = ccp(bground.contentSize.width/2, size.height/2);// Middle of image
        int width = [bground boundingBox].size.width;
        
        next = [CCSprite spriteWithFile:@"background1.png"];
        next.rotation = 0;
        next.position = ccp(bground.contentSize.width/2 + width, size.height/2);
        [self addChild:bground];
        [self addChild:next];
    }
    return self;
}
- (void) dealloc
{
	// in case you have something to dealloc, do it in this method
	// in this particular example nothing needs to be released.
	// cocos2d will automatically release all the children (Label)
	
	// don't forget to call "super dealloc"
	[super dealloc];
}
- (void) move: (int) speed
{
    int width = [bground boundingBox].size.width;
    bground.position = ccp(bground.position.x - (speed*.4), bground.position.y);
    next.position = ccp(next.position.x - (speed*.4), next.position.y);
    if (bground.position.x+width/2 <= 0)
    {
        bground.position = ccp(next.position.x + width, next.position.y);
    }
    if (next.position.x+width/2 <=0)
    {
        next.position = ccp(bground.position.x + width, bground.position.y);
    }
    
    
}
@end
