//
//  Player.m
//  Magnet Runner
//
//  Created by Alexa Rucks on 2/17/14.
//  Copyright (c) 2014 ITP382. All rights reserved.
//

#import "Player.h"
#import "GameData.h"

static Player *player = nil;

@implementation Player
@synthesize numShrapnelHit = shrapnelCount;
+(Player *) GetPlayer
{
    if(player == nil)
    {
        player = [[Player alloc] init];
    }
    return player;
}
-(Player *) init
{
    if(self = [super init])
    {
        //Load in game data
        data = [GameData GetGameData];
        
        // Define the dynamic body.
        //Set up a 1m squared box in the physics world
        b2World *world = [data GetWorld];
        bodyDef.type = b2_dynamicBody;
        bodyDef.position.Set(100/PTM_RATIO, 100/PTM_RATIO);
        body = world->CreateBody(&bodyDef);
        
        // Define another box shape for our dynamic body.
        dynamicBox.SetAsBox(.5f, .5f);//These are mid points for our 1m box
        
        // Define the dynamic body fixture.
        fixtureDef.shape = &dynamicBox;
        fixtureDef.density = 1.0f;
        fixtureDef.friction = 0.3f;
        fixtureDef.userData = self;
        body->CreateFixture(&fixtureDef);
        
        //Add a joint so that the obstacle cannot move on the y axis
        b2PrismaticJointDef *jointDef = new b2PrismaticJointDef();
        jointDef->collideConnected = true;
        // lock to X-axis, relative to top wall of game
        jointDef->Initialize(body, [data GetGroundBody],
                             body->GetWorldCenter(), b2Vec2(0, 1));
        joint = world->CreateJoint(jointDef);
        
        spriteIndex = 1;
        sprite = [CCPhysicsSprite spriteWithFile:@"megaman-run-01.png"];
        [sprite setScaleX: 1.8];
        [sprite setScaleY: 1.8];
        
        [sprite setPTMRatio:PTM_RATIO];
        [sprite setB2Body:body];
        [sprite setPosition: ccp( 100, 100)];
    }
    return self;
}
-(void) update:(ccTime) dt
{
    spriteIndex++;
    if(spriteIndex == 12)
    {
        spriteIndex = 1;
    }
    if(spriteIndex < 10)
    {
        NSString *filename = [NSString stringWithFormat:@"megaman-run-0%d.png",spriteIndex];
        [sprite initWithFile:filename];
        [sprite setScaleX: 1.8];
        [sprite setScaleY: 1.8];
    }
    else
    {
        NSString *filename = [NSString stringWithFormat:@"megaman-run-%d.png",spriteIndex];
        [sprite initWithFile:filename];
        [sprite setScaleX: 1.8];
        [sprite setScaleY: 1.8];
    }
    body->SetGravityScale(data.speed*0.2f+shrapnelCount*0.5f);
    b2World *world =[data GetWorld];
    b2Vec2 gravity = world->GetGravity();
    if (gravity.y < 0)
    {
        sprite.scaleY = 1.8;
    }
    else{
        sprite.scaleY = -1.8;
    }
}
-(CCPhysicsSprite *) GetSprite
{
    if(sprite == nil)
    {
        CCSpriteBatchNode *parent = [CCSpriteBatchNode batchNodeWithFile:@"megaman-run-01.png" capacity:100];
		CCTexture2D *spriteTexture_ = [parent texture];
        int idx = (CCRANDOM_0_1() > .5 ? 0:1);
        int idy = (CCRANDOM_0_1() > .5 ? 0:1);
        sprite = [CCPhysicsSprite spriteWithTexture:spriteTexture_ rect:CGRectMake(32 * idx,32 * idy,32,32)];
        
        [sprite setPTMRatio:PTM_RATIO];
        [sprite setB2Body:body];
        [sprite setPosition: ccp( 100, 100)];
    }
    return sprite;
}
-(void) hitShrapnel
{
    shrapnelCount++;
}
-(b2Body *) GetBody
{
    return body;
}
-(NSString *) GetType
{
    return @"player";
}
-(void) clean
{
    [sprite removeFromParentAndCleanup:YES];
    sprite = nil;
    player = nil;
}
-(void) stop
{
    b2Vec2 velocity = body->GetLinearVelocity();
    velocity.operator*=(.5f);
    body->SetLinearVelocity(velocity);
}
-(void) dealloc
{
    b2World *world = [data GetWorld];
    world->DestroyJoint(joint);
    [super dealloc];
}
@end
