//
//  GameData.h
//  Magnet Runner
//
//  Created by Alexa Rucks on 2/16/14.
//  Copyright (c) 2014 ITP382. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Box2d.h"
#import "GLES-Render.h"

//Pixel to metres ratio. Box2D uses metres as the unit for measurement.
//This ratio defines how many pixels correspond to 1 Box2D "metre"
//Box2D is optimized for objects of 1x1 metre therefore it makes sense
//to define the ratio so that your most common object type is 1x1 metre.
#define PTM_RATIO 32

@class GameplayScene;
class customCollisionListener;

@interface GameData : NSObject
{
    float gameSpeed;
    int currentScore;
    double currentTime;
    double currentDistance;
    enum GameMode {
        None, MaxDistance
    };
    enum GameMode currentMode;
    b2World* world;
    GLESDebugDraw *m_debugDraw;
    customCollisionListener *collisionListener;
    NSMutableArray * hslist;
    NSMutableArray * hsNameslist;
    NSMutableArray * playerlist;
    NSMutableArray * stringlist;
    
    
    b2Body* groundBody;
    GameplayScene *gps;
}
@property float speed;
@property int score;
@property double time;
@property double distance;
@property enum GameMode mode;
//Method required to ensure GameData is used as a Singleton
+(GameData *) GetGameData;
-(b2World*) GetWorld;
-(b2Body*) GetGroundBody;
-(GameplayScene *) GetGameplayScene;
-(void) SetGameplayScene:(GameplayScene *)gp;
-(void) clean;
-(void) check_score;
//-(GLESDebugDraw *) GetDebugDraw;
@end
