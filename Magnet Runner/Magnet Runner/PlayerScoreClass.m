//
//  PlayerScoreClass.m
//  Magnet Runner
//
//  Created by Edward Foux on 3/8/14.
//  Copyright (c) 2014 ITP382. All rights reserved.
//

#import "PlayerScoreClass.h"


@implementation PlayerScoreClass


- (id)init
{
    self = [super init];
    if (self) {
        name=[NSString stringWithFormat:@""];
        score=0;
        
    }
    return self;
}

-(void)setName:(NSString *)namePlayer{
    
    name=[NSString stringWithFormat:@"%@",namePlayer];
    
}
-(void)setScore:(NSNumber *)scorePlayer{
    
    score=scorePlayer;
    
}
-(NSString *)getTheName{
    
    return name;
}

-(NSNumber *)getTheScore{
    
    return score;
    
}

@end
