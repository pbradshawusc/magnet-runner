//
//  customCollisionListener.h
//  Magnet Runner
//
//  Created by Alexa Rucks on 2/21/14.
//  Copyright (c) 2014 ITP382. All rights reserved.
//

#import "Box2D.h"
#import <vector>
#import <algorithm>
#import "GameplayScene.h"

/*struct MyContact {
    b2Fixture *fixtureA;
    b2Fixture *fixtureB;
    bool operator==(const MyContact& other) const
    {
        return (fixtureA == other.fixtureA) && (fixtureB == other.fixtureB);
    }
};*/

class customCollisionListener : public b2ContactListener {
private:
    GameplayScene *gpScene;
    
public:
    //std::vector<MyContact>_contacts;
    
    customCollisionListener();
    ~customCollisionListener();
    
    virtual void BeginContact(b2Contact* contact);
    virtual void EndContact(b2Contact* contact);
    virtual void PreSolve(b2Contact* contact, const b2Manifold* oldManifold);
    virtual void PostSolve(b2Contact* contact, const b2ContactImpulse* impulse);
    
    void setGameplayScene(GameplayScene *gp);
    
};
