//
//  GameData.m
//  Magnet Runner
//
//  Created by Alexa Rucks on 2/16/14.
//  Copyright (c) 2014 ITP382. All rights reserved.
//

#import "GameData.h"
#import "GameplayScene.h"
#import "customCollisionListener.h"
#import "PlayerScoreClass.h"

//Required declaration of imiatation "class" variable
static GameData *pointer = nil;


@implementation GameData
@synthesize speed = gameSpeed;
@synthesize score = currentScore;
@synthesize time = currentTime;
@synthesize distance = currentDistance;
@synthesize mode = currentMode;
//Class Method to get a constant pointer to the Singleton Game Data
+(GameData *) GetGameData
{
    if (pointer == nil)
    {
        pointer = [[GameData alloc] init];
    }
    return pointer;
}
-(id) init
{
    if(self = [super init])
    {
        gameSpeed = 10;
        currentScore = 0;
        currentTime = 0;
        currentDistance = 0;
        currentMode = None;
        hslist = [[NSMutableArray alloc] init];
        playerlist=[[NSMutableArray alloc]init];
        stringlist=[[NSMutableArray alloc]init];
        for(int i = 0; i < 5; i++)
            
        {
            
            
            
            NSString *key;
            
            NSString *keyName;
            
            
            
            key = [NSString stringWithFormat:@"HighScoreIndex%d", i];
            
            keyName = [NSString stringWithFormat:@"HighScoreIndexName%d", i];

            
            
            
            NSNumber *num = [NSNumber numberWithInt:[[NSUserDefaults standardUserDefaults] integerForKey:key]];
            
            
            
            //for storing the names in the player objects
            NSString *name = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:keyName]];
            
            
            NSLog(@"%@ is the name", name);
            
            
            NSLog(@" the player is %@",name);
            
            
            
            if (!name)
                {
                name=@"no name";
                NSLog(@"creating the empty player");
                }
            
            if (!num){
                num=0;
            }
            
            
            [hslist addObject:num];
            [stringlist addObject:name];

            
            
            
        }        [self initPhysics];
    }
    return self;
}

-(void)removeLastObjects:(NSMutableArray *)array{
    
    while([array count]>5)
        [array removeLastObject];
    
}
-(void) initPhysics
{
    CGSize s = [[CCDirector sharedDirector] winSize];
	
	b2Vec2 gravity;
	gravity.Set(0.0f, -10.0f);
	world = new b2World(gravity);
	
	
	// Do we want to let bodies sleep?
	world->SetAllowSleeping(false);
	
	world->SetContinuousPhysics(true); //We want to use tunneling rather than allowing bodies to sleep since the movement speed will increase until the player crashes. This will allow for higher speeds and thinner objects
	
	//THIS CODE DRAWS THE DEBUG BOUNDING BOXES, REMOVE IT FOR THE FINAL PLAYTHROUGH
    /*
    //m_debugDraw = new GLESDebugDraw( PTM_RATIO );
    //world->SetDebugDraw(m_debugDraw);
	
    uint32 flags = 0;
    flags += b2Draw::e_shapeBit;
    //		flags += b2Draw::e_jointBit;
    //		flags += b2Draw::e_aabbBit;
    //		flags += b2Draw::e_pairBit;
    //		flags += b2Draw::e_centerOfMassBit;
    //m_debugDraw->SetFlags(flags);
     */
	//End Standard Box2D debugDrawing code
	
	// Define the ground body, Only for use in first round testing, ground will eventually become an obstacle coming toward the player, allowing for gaps the player can fall through
    b2BodyDef groundBodyDef;
    groundBodyDef.position.Set(0, 0); // bottom-left corner
	
    // Call the body factory which allocates memory for the ground body
    // from a pool and creates the ground box shape (also from a pool).
    // The body is also added to the world.
    groundBody = world->CreateBody(&groundBodyDef);
	
    // Define the ground box shape.
    b2EdgeShape groundBox;
	
    // bottom
	
    groundBox.Set(b2Vec2(0,0), b2Vec2((s.width+1000)/PTM_RATIO,0));
    groundBody->CreateFixture(&groundBox,0);
	
    // top
    groundBox.Set(b2Vec2(0,s.height/PTM_RATIO), b2Vec2((s.width+1000)/PTM_RATIO,s.height/PTM_RATIO));
    groundBody->CreateFixture(&groundBox,0);
    
    collisionListener = new customCollisionListener();
    world->SetContactListener(collisionListener);
}
-(b2World *)GetWorld
{
    return world;
}
-(b2Body *) GetGroundBody
{
    return groundBody;
}
-(GameplayScene *) GetGameplayScene
{
    return gps;
}
-(void) SetGameplayScene:(GameplayScene *)gp
{
    gps = gp;
    collisionListener->setGameplayScene(gp);
}
-(void) clean
{
    gameSpeed = 10;
    currentScore = 0;
    currentTime = 0;
    currentDistance = 0;
    currentMode = None;
    [self initPhysics];
}
-(void) check_score
{
    NSLog( @"%@", [[NSUserDefaults standardUserDefaults] dictionaryRepresentation] );
    currentScore = (currentDistance*currentDistance/currentTime)*([Player GetPlayer].numShrapnelHit+1);
    
    NSLog(@"currentScore:%d", currentScore);
    
    
    
    NSNumber *scoreNum = [NSNumber numberWithInt:currentScore];
    
    NSString *key = [NSString stringWithFormat:@"playername"];
    
    NSString *val = [[NSUserDefaults standardUserDefaults]
                     
                     stringForKey:key];

    int count=0;
    bool canInsert=true;
    while (count<3 && canInsert){
        int score=[[hslist objectAtIndex:count] integerValue];
        if (score<currentScore){
            
            for (int i=3;i>count; i--){
                
                [hslist insertObject:[hslist objectAtIndex:i] atIndex:i+1];
                [stringlist insertObject:[stringlist objectAtIndex:i] atIndex:i+1];
            }
            
            [hslist insertObject:scoreNum atIndex:count];
            [stringlist insertObject:val atIndex:count];
            canInsert=false;
        }
        count++;
    }
    if (canInsert && count<4){
        [hslist insertObject:scoreNum atIndex:count];
        [stringlist insertObject:val atIndex:count];
    }
    
    
    
    if ([hslist count] > 5)
        
    {
        
        [self removeLastObjects:hslist];
        [self removeLastObjects:stringlist];
        
    }
    
    for(int i=0; i<5;i++){
        
        
        
        NSString *key = [NSString stringWithFormat:@"HighScoreIndex%d", i];
        
        NSString *keyname = [NSString stringWithFormat:@"HighScoreIndexName%d", i];
        
        [[NSUserDefaults standardUserDefaults] setObject:[hslist objectAtIndex:i] forKey:key];
        
        [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%@",[stringlist objectAtIndex:i]]  forKey:keyname];
        
    }
   
    
}
-(void) dealloc
{
    delete collisionListener;
    delete world;
    [super dealloc];
}
@end
