//
//  MainMenuScene.m
//  Magnet Runner
//
//  Created by Edward Foux on 2/19/14.
//  Copyright (c) 2014 ITP382. All rights reserved.
//

#import "MainMenuScene.h"
#import "GameplayScene.h"
#import "AppDelegate.h"
#import "HighScoreListScene.h"
#import "OptionsMenu.h"
#import "SimpleAudioEngine.h"

@class TutorialScene;

@implementation MainMenuScene



// Helper class method that creates a Scene with the MainMenuLayer as the only child.
+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	MainMenuScene *layer = [MainMenuScene node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}


// on "init" you need to initialize your instance
-(id) init
{
   

	// always call "super" init
	// Apple recommends to re-assign "self" with the "super's" return value
	if( (self=[super init]) )
	{
        if(![[SimpleAudioEngine sharedEngine] isBackgroundMusicPlaying])
        {
            [[SimpleAudioEngine sharedEngine] playBackgroundMusic:@"Fighting of the Spirits TOS.mp3" loop:YES];
        }
		
		// ask director for the window size
		CGSize size = [[CCDirector sharedDirector] winSize];
		
        
        //Add the background image of the magnet runner 
		CCSprite *background;
		background = [CCSprite spriteWithFile:@"background1.png"];
		background.rotation = 90;
		
        if (size.height>size.width){
            background.position = ccp(size.height/2, size.width/2);
        }
        else{
            background.position = ccp( size.width/2,size.height/2);
        }
		
		// add the image as a child to this Layer
		[self addChild: background];
		
		//
		// Game Title
		//
        CCLabelTTF * titlelabel;
        titlelabel = [CCLabelTTF labelWithString:@"Magnet Runner" fontName:@"Marker Felt" fontSize:44];
        
        [titlelabel setColor:ccc3(255,255,255)];
        titlelabel.position = ccp (size.width/2, size.height-100);
        [self addChild:titlelabel z:0];

		
		// Default font size will be 28 points.
		[CCMenuItemFont setFontSize:28];
		
		// Main Menu Buttons
		CCMenuItem *itemNewGame = [CCMenuItemFont itemWithString:@"New Game" block:^(id sender) {
            
            [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:0.5 scene:[GameplayScene node]]];
			
		}];
        
        CCMenuItem *highScores = [CCMenuItemFont itemWithString:@"High Scores" block:^(id sender) {
            
            [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:0.5 scene:[HighScoreListScene node]]];
			
		}];
        
        CCMenuItem *optionsMenu = [CCMenuItemFont itemWithString:@"Options" block:^(id sender) {
            
            [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:0.5 scene:[OptionsMenu node]]];
			
		}];
        
		
		CCMenu *menu = [CCMenu menuWithItems:itemNewGame,highScores,optionsMenu, nil];
		
		[menu alignItemsVerticallyWithPadding:20];
		//[menu setPosition:ccp( size.height/2, size.width/2 - 125)];
        if (size.height>size.width){
            [menu setPosition:ccp(size.height/2, size.width/2-50)];
        }
        else{
            [menu setPosition:ccp( size.width/2,size.height/2-50)];
        }
		
		// Add the menu to the layer
		[self addChild:menu];
        
        NSString *key = [NSString stringWithFormat:@"playername"];
        NSString *val = [[NSUserDefaults standardUserDefaults]
                         stringForKey:key];
        
        /*if (val){
            NSLog(@"Do you want to play as %@",val);
            [self resetName];
            
        }else{
            
            [self editname];
            
        }*/
        
        if(!val)
        {
            [self editname];
        }
        
       
         val = [[NSUserDefaults standardUserDefaults]
                         stringForKey:key];  
        
       // NSLog(@"final name %@",val);
     
        
        
        
	}
	return self;
 

}




-(void)editname{
    
    
    UIAlertView *textFieldAlert = [[UIAlertView alloc] initWithTitle:@"Please enter your name" message:@"" delegate:self cancelButtonTitle:@"Done" otherButtonTitles:nil
                                   ];
    
    [textFieldAlert addTextFieldWithValue:@"" label:@"ENTER YOUR NAME HERE"];
    textField=[textFieldAlert textFieldAtIndex:0];
    textField.keyboardType=UIKeyboardTypeAlphabet;
    textField.clearsOnBeginEditing=YES;
    textField.clearButtonMode=UITextFieldViewModeWhileEditing;
    textField.keyboardAppearance=UIKeyboardAppearanceAlert;
    
    
    [textFieldAlert show];
    [textFieldAlert release];
    
}

                                                                            
-(void)resetName{
    
    NSString *key = [NSString stringWithFormat:@"playername"];
    NSString *val = [[NSUserDefaults standardUserDefaults]
                     stringForKey:key];

    UIAlertView *textFieldAlert = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"Do you want to play as %@?",val] message:@"" delegate:self cancelButtonTitle:@"Reset" otherButtonTitles:@"Keep the same",nil
                                   ];
    
    
    
    
    [textFieldAlert show];
    [textFieldAlert release];
    
}
                                                                            
                                                                            

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    NSString * buttonOntitle=[alertView buttonTitleAtIndex:buttonIndex];
    if ([buttonOntitle isEqualToString:@"Done"]){
        NSLog(@"entered name: %@",textField.text);
        
        NSString *key = [NSString stringWithFormat:@"playername"];
        [[NSUserDefaults standardUserDefaults] setObject:textField.text forKey:key];
        NSString *val = [[NSUserDefaults standardUserDefaults]
                         stringForKey:key];
        NSLog(@"the stored name is %@", val);
        
        [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:0.5 scene:[TutorialScene node]]];
    }else if ([buttonOntitle isEqualToString:@"Reset"]){
         NSString *key = [NSString stringWithFormat:@"playername"];
        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:key];
        NSString *val = [[NSUserDefaults standardUserDefaults]
                         stringForKey:key];
        NSLog(@"the name is reset and now is  %@", val);
        
        [self editname];
       
    }
    
   
    
    
}
// on "dealloc" you need to release all your retained objects
- (void) dealloc
{
	// in case you have something to dealloc, do it in this method
	// in this particular example nothing needs to be released.
	// cocos2d will automatically release all the children (Label)
    
    
	
	// don't forget to call "super dealloc"
	[super dealloc];
}



@end
