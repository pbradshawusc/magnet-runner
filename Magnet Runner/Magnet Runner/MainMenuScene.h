//
//  MainMenuScene.h
//  Magnet Runner
//
//  Created by Edward Foux on 2/19/14.
//  Copyright (c) 2014 ITP382. All rights reserved.
//

#import "CCScene.h"
#import "cocos2d.h"

@interface MainMenuScene : CCScene
{
    UITextField* textField;
}
// returns a CCScene that contains the HelloWorldLayer as the only child
+(CCScene *) scene;

@end
