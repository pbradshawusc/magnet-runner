//
//  Obstacle.m
//  Magnet Runner
//
//  Created by Alexa Rucks on 2/19/14.
//  Copyright (c) 2014 ITP382. All rights reserved.
//

#import "Obstacle.h"

@implementation Obstacle
-(Obstacle *) initWithWidth:(int)w Height:(int) h X:(int)x Top:(bool) t
{
    if(self = [super init])
    {
        destroyed = false;
        
        //Initialize the width and height
        width = w;
        height = h;
        
        //Load in game data
        data = [GameData GetGameData];
        
        // ask director for the window size
		CGSize size = [[CCDirector sharedDirector] winSize];
        
        // Define the dynamic body.
        //Set up a 1m squared box in the physics world
        b2World *world = [data GetWorld];
        bodyDef.type = b2_dynamicBody;
        if(t)
        {
            bodyDef.position.Set((size.width+x)/PTM_RATIO, (size.height-(height/2))/PTM_RATIO);
        }
        else
        {
            bodyDef.position.Set((size.width+x)/PTM_RATIO, (height/2)/PTM_RATIO);
        }
        body = world->CreateBody(&bodyDef);
        
        // Define another box shape for our dynamic body.
        dynamicBox.SetAsBox(width/(PTM_RATIO * 2.0f), height/(PTM_RATIO * 2.0f));//These are mid points for our 1m box
        
        // Define the dynamic body fixture.
        fixtureDef.shape = &dynamicBox;
        fixtureDef.density = 0.0f;
        fixtureDef.friction = 0.0f;
        fixtureDef.userData = self;
        body->CreateFixture(&fixtureDef);
        body->SetGravityScale(0);
        body->SetLinearVelocity(b2Vec2(-data.speed,0));
        
        //Add a joint so that the obstacle cannot move on the y axis
        b2PrismaticJointDef *jointDef = new b2PrismaticJointDef();
        jointDef->collideConnected = true;
        // lock to X-axis, relative to top wall of game
        jointDef->Initialize(body, [data GetGroundBody],
                            body->GetWorldCenter(), b2Vec2(1, 0));
        joint = world->CreateJoint(jointDef);
        
        sprite = [CCPhysicsSprite spriteWithFile:@"Metal Wall.jpg"];
        [sprite setScaleX:width/sprite.contentSize.width];
        [sprite setScaleY:height/sprite.contentSize.height];
        //[parent addChild:sprite];
        
        [sprite setPTMRatio:PTM_RATIO];
        [sprite setB2Body:body];
        if(t)
        {
            [sprite setPosition: ccp( size.width + x, size.height-(height/2))];
        }
        else
        {
            [sprite setPosition: ccp( size.width + x, height/2)];
        }
    }
    return self;
}
-(CCPhysicsSprite *) getSprite
{
    return sprite;
}
-(NSString *) toString
{
    NSString *tostr = [NSString stringWithFormat:@"%i, %d", width, height];
    return tostr;
}
-(bool) checkBounds
{
    if(!body)
    {
        return false;
    }
    if(body->GetPosition().x < -width)
    {
        return true;
    }
    return false;
}
-(b2Body *) GetBody
{
    return body;
}
-(NSString *) GetType
{
    return @"obstacle";
}
-(void) destroy
{
    if(destroyed){
        return;
    }
    NSLog(@"Destroying Obstacle");
    destroyed = true;
    //[sprite removeFromParentAndCleanup:YES];
    b2World *world = [data GetWorld];
    body->SetTransform(b2Vec2(-1000,1000), 0);
    body->DestroyFixture(&(body->GetFixtureList()[0]));
    body->SetUserData(nil);
    world->DestroyBody(body);
    [sprite removeFromParentAndCleanup:YES];
}

-(void) dealloc
{
    b2World *world = [data GetWorld];
    world->DestroyJoint(joint);
    //[sprite removeFromParentAndCleanup:true];
    world->DestroyBody(body);
    [super dealloc];
}
@end
