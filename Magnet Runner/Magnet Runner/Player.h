//
//  Player.h
//  Magnet Runner
//
//  Created by Alexa Rucks on 2/17/14.
//  Copyright (c) 2014 ITP382. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "Box2D.h"
#import "CCPhysicsSprite.h"

@class GameData;

@interface Player : NSObject
{
    b2BodyDef bodyDef;
    b2Body *body;
    b2PolygonShape dynamicBox;
    b2FixtureDef fixtureDef;
    CCPhysicsSprite *sprite;
    GameData *data;
    b2Joint *joint;
    int shrapnelCount;
    int spriteIndex;
}
@property int numShrapnelHit;
+(Player *) GetPlayer;
-(CCPhysicsSprite *) GetSprite;
-(void) update:(ccTime) dt;
-(void) hitShrapnel;
-(b2Body *) GetBody;
-(void) clean;
-(void) stop;
-(NSString *) GetType;
@end
