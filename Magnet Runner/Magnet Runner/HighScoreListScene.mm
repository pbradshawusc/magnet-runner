//
//  HighScoreListScene.m
//  Magnet Runner
//
//  Created by Edward Foux on 2/24/14.
//  Copyright (c) 2014 ITP382. All rights reserved.
//

#import "HighScoreListScene.h"
#import "AppDelegate.h"
#import "MainMenuScene.h"

@implementation HighScoreListScene

CGSize size;


// Helper class method that creates a Scene with the MainMenuLayer as the only child.
+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	MainMenuScene *layer = [MainMenuScene node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}


// on "init" you need to initialize your instance
-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super's" return value
	if( (self=[super init]) )
	{
		
		// ask director for the window size
		 size = [[CCDirector sharedDirector] winSize];
		
        
        //Add the background image of the magnet runner 
		CCSprite *background;
		background = [CCSprite spriteWithFile:@"background1.png"];
		background.rotation = 90;
		
        if (size.height>size.width){
            background.position = ccp(size.height/2, size.width/2);
        }
        else{
            background.position = ccp( size.width/2,size.height/2);
        }
		
		// add the image as a child to this Layer
		[self addChild: background];
		
		//
		// Leaderboards and Achievements
		//
		
		// Default font size will be 28 points.
		[CCMenuItemFont setFontSize:28];
		
		// Achievement Menu Item using blocks
		CCMenuItem *itemNewGame = [CCMenuItemFont itemWithString:@"Back" block:^(id sender) {
            
            [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:0.5 scene:[MainMenuScene node]]];
			
		}];
        
        
    /*    NSString *filePath=[self pathOfFile];
        NSLog(@"%@",filePath);
        if ([[NSFileManager defaultManager]fileExistsAtPath:filePath]){
            NSMutableArray *array=[[NSMutableArray alloc]initWithContentsOfFile:filePath];
            for (int i; i<[array count]; i++){
                
                
                
            }
            
            [array release];
        }
        */
        NSMutableArray * hslist = [[NSMutableArray alloc] init];
        for(int i = 0; i < 5; i++)
        {
            //NSLog(@"%@", [hslist objectAtIndex:i]);
            NSString *key = [NSString stringWithFormat:@"HighScoreIndex%d", i];
            NSString *nameKey = [NSString stringWithFormat:@"HighScoreIndexName%d", i];

            
            
            NSNumber *num = [NSNumber numberWithInt:[[NSUserDefaults standardUserDefaults] integerForKey:key]];
            NSString *playerName = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:nameKey]];
            [hslist addObject:num];
            if(num.intValue > 0){
                key = [NSString stringWithFormat:@"HighScoreName%@", num];
                NSLog(@"%@", num);
                
            
                CCLabelTTF *m_RoundLabel = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"%@ %@",playerName,num] fontName:@"Marker Felt" fontSize:30];
                m_RoundLabel.position = ccp(size.width/2, size.height-30-i*30); m_RoundLabel.visible = YES;
                [self addChild: m_RoundLabel];
            }

            
        }
        
		
		CCMenu *menu = [CCMenu menuWithItems:itemNewGame, nil];
		
		[menu alignItemsVerticallyWithPadding:20];
		//[menu setPosition:ccp( size.height/2, size.width/2 - 125)];
        if (size.height>size.width){
            [menu setPosition:ccp(size.height/2, size.width/2-50)];
        }
        else{
            [menu setPosition:ccp( size.width/2,size.height/2-50)];
        }
		
		// Add the menu to the layer
		[self addChild:menu];
        
        
	}
	return self;
}

// on "dealloc" you need to release all your retained objects
- (void) dealloc
{
	// in case you have something to dealloc, do it in this method
	// in this particular example nothing needs to be released.
	// cocos2d will automatically release all the children (Label)
	
	// don't forget to call "super dealloc"
	[super dealloc];
}

-(NSString *)pathOfFile{
    NSArray *path=NSSearchPathForDirectoriesInDomains(NSDocumentationDirectory, NSUserDomainMask, YES);
    NSString *documentFolder=[path objectAtIndex:0];
    return [documentFolder stringByAppendingFormat:@"highscores.plist"];
    
         
}





@end
