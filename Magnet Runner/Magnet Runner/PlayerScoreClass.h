//
//  PlayerScoreClass.h
//  Magnet Runner
//
//  Created by Edward Foux on 3/8/14.
//  Copyright (c) 2014 ITP382. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface PlayerScoreClass : NSObject
{
    NSString * name;
    NSNumber * score;
    
}


-(void) setName:(NSString *)name;
-(void) setScore:(NSNumber *)score;
-(NSString *) getTheName;
-(NSNumber *) getTheScore;


@end
