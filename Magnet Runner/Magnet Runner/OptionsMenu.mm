//
//  OptionsMenu.m
//  Magnet Runner
//
//  Created by Alexa Rucks on 3/8/14.
//  Copyright (c) 2014 ITP382. All rights reserved.
//

#import "OptionsMenu.h"
#import "AppDelegate.h"
#import "SkinMenuScene.h"
#import "TutorialScene.h"
#import "cocos2d.h"

//Most code copied from MainMenuScene - basic menu

@class MainMenuScene;

@implementation OptionsMenu
+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	OptionsMenu *layer = [OptionsMenu node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}
-(id) init
{
    
    
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super's" return value
	if( (self=[super init]) )
	{
		
		// ask director for the window size
		CGSize size = [[CCDirector sharedDirector] winSize];
		
        
        //Add the background image of the magnet runner
		CCSprite *background;
		background = [CCSprite spriteWithFile:@"background1.png"];
		background.rotation = 90;
		
        if (size.height>size.width){
            background.position = ccp(size.height/2, size.width/2);
        }
        else{
            background.position = ccp( size.width/2,size.height/2);
        }
		
		// add the image as a child to this Layer
		[self addChild: background];
		
		
		// Default font size will be 28 points.
		[CCMenuItemFont setFontSize:28];
		
		// Achievement Menu Item using blocks
        CCMenuItem *resetName = [CCMenuItemFont itemWithString:@"Reset Name" block:^(id sender) {
            
            [self resetName];
			
		}];
        
        CCMenuItem *skinSelector = [CCMenuItemFont itemWithString:@"Skins" block:^(id sender) {
            
            [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:0.5 scene:[SkinMenuScene node]]];
			
		}];
        
        CCMenuItem *back = [CCMenuItemFont itemWithString:@"Return to Main Menu" block:^(id sender) {
            
            [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:0.5 scene:[MainMenuScene node]]];
			
		}];
        CCMenuItem *tutorial = [CCMenuItemFont itemWithString:@"Play Tutorial" block:^(id sender) {
            
            [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:0.5 scene:[TutorialScene node]]];
			
		}];
        
		
		CCMenu *menu = [CCMenu menuWithItems: tutorial, skinSelector, resetName, back, nil];
		
		[menu alignItemsVerticallyWithPadding:20];
		//[menu setPosition:ccp( size.height/2, size.width/2 - 125)];
        if (size.height>size.width){
            [menu setPosition:ccp(size.height/2, size.width/2-30)];
        }
        else{
            [menu setPosition:ccp( size.width/2,size.height/2-30)];
        }
		
		// Add the menu to the layer
		[self addChild:menu];
        
        CCLabelTTF * titlelabel;
        titlelabel = [CCLabelTTF labelWithString:@"Options" fontName:@"Marker Felt" fontSize:36];
        
        [titlelabel setColor:ccc3(255,255,255)];
        titlelabel.position = ccp (size.width/2, size.height-titlelabel.contentSize.height);
        [self addChild:titlelabel z:0];
        
        NSString *key = [NSString stringWithFormat:@"playername"];
        NSString *val = [[NSUserDefaults standardUserDefaults]
                         stringForKey:key];
        
        if(!val)
        {
            [self editname];
        }
        
        
        val = [[NSUserDefaults standardUserDefaults]
               stringForKey:key];
        
	}
	return self;
    
    
}

-(void)editname{
    
    
    UIAlertView *textFieldAlert = [[UIAlertView alloc] initWithTitle:@"Please enter your name" message:@"" delegate:self cancelButtonTitle:@"Done" otherButtonTitles:nil
                                   ];
    
    [textFieldAlert addTextFieldWithValue:@"" label:@"ENTER YOUR NAME HERE"];
    textField=[textFieldAlert textFieldAtIndex:0];
    textField.keyboardType=UIKeyboardTypeAlphabet;
    textField.clearsOnBeginEditing=YES;
    textField.clearButtonMode=UITextFieldViewModeWhileEditing;
    textField.keyboardAppearance=UIKeyboardAppearanceAlert;
    
    
    [textFieldAlert show];
    [textFieldAlert release];
    
}


-(void)resetName{
    
    NSString *key = [NSString stringWithFormat:@"playername"];
    NSString *val = [[NSUserDefaults standardUserDefaults]
                     stringForKey:key];
    
    UIAlertView *textFieldAlert = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"Do you want to play as %@?",val] message:@"" delegate:self cancelButtonTitle:@"Reset" otherButtonTitles:@"Keep the same",nil];
    
    [textFieldAlert show];
    [textFieldAlert release];
    
}



-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    NSString * buttonOntitle=[alertView buttonTitleAtIndex:buttonIndex];
    if ([buttonOntitle isEqualToString:@"Done"]){
        NSLog(@"entered name: %@",textField.text);
        
        NSString *key = [NSString stringWithFormat:@"playername"];
        [[NSUserDefaults standardUserDefaults] setObject:textField.text forKey:key];
        NSString *val = [[NSUserDefaults standardUserDefaults]
                         stringForKey:key];
        NSLog(@"the stored name is %@", val);
    }else if ([buttonOntitle isEqualToString:@"Reset"]){
        NSString *key = [NSString stringWithFormat:@"playername"];
        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:key];
        NSString *val = [[NSUserDefaults standardUserDefaults]
                         stringForKey:key];
        NSLog(@"the name is reset and now is  %@", val);
        
        [self editname];
        
    }
}
@end
