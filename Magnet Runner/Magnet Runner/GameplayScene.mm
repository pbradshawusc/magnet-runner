//
//  GameplayScene.m
//  Magnet Runner
//
//  Created by Alexa Rucks on 2/16/14.
//  Copyright (c) 2014 ITP382. All rights reserved.
//

#import "GameplayScene.h"
#import "ObstacleFactory.h"
#import "MainMenuScene.h"
#import "GameOverScene.h"
#import "Shrapnel.h"
#import "DestructibleObstacle.h"
#import "SimpleAudioEngine.h"
#import "SpriteLayer.h"

@implementation GameplayScene
-(id) initNew
{
    self = [super init];
    return self;
}
-(id) init
{
    if(self = [super init])
    {
        [[SimpleAudioEngine sharedEngine] playBackgroundMusic:@"Full Force TOS.mp3" loop:YES];
        
        //Read in the current Game Data and initialize where needed
        gameData = [GameData GetGameData];
        gameData.time = 0;
        gameData.score = 0;
        gameData.speed = 4;
        [gameData SetGameplayScene:self];
        
        //Read in/Create the player data
        player = [Player GetPlayer];
        
        shrapnel = [[NSMutableArray alloc] init];
        
        //Adding a Background
        backgroundlayer = [Background node];
        
        //Displays score
        CGSize s = [CCDirector sharedDirector].winSize;
        NSString* a = [NSString stringWithFormat:@"Score: %d", gameData.score];
        scorelabel = [CCLabelTTF labelWithString:a fontName:@"Marker Felt" fontSize:24];
        [scorelabel setColor:ccc3(255,255,255)];
        scorelabel.position = ccp (scorelabel.contentSize.width/2, s.height-scorelabel.contentSize.height);
        [self addChild:scorelabel z:1];
        
        //Displays Instructions
        label = [CCLabelTTF labelWithString:@"Tap Screen To Flip Gravity\nAvoid Obstacles" fontName:@"Marker Felt" fontSize:24];
		[self addChild:label z:1];
		[label setColor:ccc3(255,255,255)];
		label.position = ccp( s.width/2, s.height-(label.contentSize.height/2));
        
        //Create and add the sprite layer where the Box2D world is emulated
        spriteLayer = [SpriteLayer node];
        [self addChild:spriteLayer z:0];
        
        //Initialize the obstacle array
        obstacles = nil;
        obstacleTime = 0;
        playedSound = false;
        [self addChild:backgroundlayer z:-1];
        
        levelNum = 0;
        
        //Schedule the scene for updates to update all layers within
        [self scheduleUpdate];
    }
    return self;
}
-(void) update:(ccTime)dt
{
    [spriteLayer update:dt];
    for(NSInteger i = [shrapnel count]-1; i >= 0; i--)
    {
        Shrapnel *s = [shrapnel objectAtIndex:i];
        [s update:dt];
        if([s hasCollided])
        {
            [s destroy];
            [shrapnel removeObjectAtIndex:i];
            
        }
    }
    [spriteLayer checkDest];
    gameData.time += dt;
    gameData.distance += gameData.speed*dt;
    gameData.score = (gameData.distance*gameData.distance/gameData.time)*(player.numShrapnelHit+1);
    
    if(gameData.distance > (600/PTM_RATIO) && label!=nil){
        [self removeChild:label cleanup:YES];
        label = nil;
    }
    
    CGSize s = [CCDirector sharedDirector].winSize;
    if(gameData.distance > (obstacleTime + (1200)/PTM_RATIO) || obstacleTime == 0)
    {
        levelNum++;
        if(levelNum < 3){
            NSMutableArray *newObstacles;
            newObstacles = [NSMutableArray arrayWithArray:[ObstacleFactory ProcedurallyGenerateLevel]];
            obstacleTime = gameData.distance;
            if(obstacleTime == 0){
                obstacleTime = 1;
            }
            for(Obstacle *ob in newObstacles)
            {
                [spriteLayer addChild:[ob getSprite]];
            }
            [obstacles addObjectsFromArray:newObstacles];
        }
        else if(levelNum == 3)
        {
            obstacleTime = gameData.distance;
            DestructibleObstacle *dest = [[DestructibleObstacle alloc] init];
            [spriteLayer setDest:dest];
            [spriteLayer addChild:[dest getSprite]];
            [obstacles addObject:dest];
        }
        else if(levelNum == 4)
        {
            gameData.speed += 0.5f;
            playedSound = false;
            levelNum = 0;
        }
    }
    else if(levelNum == 3 && gameData.distance > obstacleTime+(300/PTM_RATIO) && !playedSound)
    {
        playedSound = true;
        [[SimpleAudioEngine sharedEngine] playEffect:@"Alert SE.mp3"];
    }
    int rand = arc4random_uniform(1000);
    if(rand > 985)
    {
        Shrapnel *s = [[Shrapnel alloc] init];
        [spriteLayer addChild:[s GetSprite]];
        [shrapnel addObject:s];
    }
    [scorelabel setString:[NSString stringWithFormat:@"Score: %d", (int)gameData.score]];
    scorelabel.position = ccp (scorelabel.contentSize.width/2, s.height-scorelabel.contentSize.height);
    [player update:dt];
    [backgroundlayer move:[gameData speed]];
    if(obstacles)
    {
        for(NSInteger i = [obstacles count]-1; i >= 0; i--)
        {
            Obstacle *o = [obstacles objectAtIndex:i];
            if([o checkBounds])
            {
                [o destroy];
                [obstacles removeObjectAtIndex:i];
            }
        }
    }
}
-(void) ObjectiveCompleted
{
    
}
-(void) destroyedWall
{
    
}
-(bool) GetCompleted
{
   return false;
}
-(NSMutableArray *) getShrapnel {
  return shrapnel;
}
-(void) removeShrapnelAt: (int) i {
    [shrapnel removeObjectAtIndex: i];
}
-(void) LoseConditionTriggered
{
    [self unscheduleUpdate];
    [[SimpleAudioEngine sharedEngine] playEffect:@"Game Over SE.mp3"];
    [[SimpleAudioEngine sharedEngine] playBackgroundMusic:@"The Grudge TOS.mp3" loop:YES];
    [gameData check_score];
    [player clean];
    //[gameData clean];
    [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:1.0 scene:[GameOverScene node] ]];
}

@end
