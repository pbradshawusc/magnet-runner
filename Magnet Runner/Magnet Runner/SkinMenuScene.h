//
//  SkinMenuScene.h
//  Magnet Runner
//
//  Created by Edward Foux on 3/6/14.
//  Copyright (c) 2014 ITP382. All rights reserved.
//



#import "CCScene.h"
#import "SpriteLayer.h"



@interface SkinMenuScene : CCScene
{
    CCLabelTTF *label;
    NSNumber * score;
    NSMutableArray * arrayOfIdle;
    NSMutableArray * arrayOfSelected;
    NSMutableArray * arrayOfMenuSprites;
	

}
// returns a CCScene that contains the HelloWorldLayer as the only child
+(CCScene *) scene;

@end
