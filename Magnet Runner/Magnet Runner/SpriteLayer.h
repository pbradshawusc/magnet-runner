//
//  SpriteLayer.h
//  Magnet Runner
//
//  Created by Alexa Rucks on 2/16/14.
//  Copyright (c) 2014 ITP382. All rights reserved.
//
#import <GameKit/GameKit.h>
#import "cocos2d.h"
#import "Box2D.h"
#import "GLES-Render.h"
#import "CCLayer.h"
#import "DestructibleObstacle.h"
#import "Shrapnel.h"

//Pixel to metres ratio. Box2D uses metres as the unit for measurement.
//This ratio defines how many pixels correspond to 1 Box2D "metre"
//Box2D is optimized for objects of 1x1 metre therefore it makes sense
//to define the ratio so that your most common object type is 1x1 metre.
//#define PTM_RATIO 32

@class GameData;
@class GameplayScene;

@interface SpriteLayer : CCLayer
{
    CCTexture2D *spriteTexture_;
    GameData *data;
    DestructibleObstacle *dest;
}
-(void) setDest:(DestructibleObstacle *)d;
-(void) checkDest;
@end
