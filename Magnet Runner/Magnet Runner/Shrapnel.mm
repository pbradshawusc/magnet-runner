//
//  Shrapnel.mm
//  Magnet Runner
//
//  Created by James Lynch on 2/19/14.
//  Copyright (c) 2014 ITP382. All rights reserved.
//

#import "Shrapnel.h"


const int DEFAULT_ATTRACTIVENESS = 10;


@implementation Shrapnel

@synthesize shrapnelAttractiveness = attractiveness;

+ (Shrapnel *) getShrapnelwithAttractiveness: (int) attractiveness {
    return [[Shrapnel alloc] initWithAttractiveness: attractiveness];
};

- (id) init {
    return [self initWithAttractiveness: DEFAULT_ATTRACTIVENESS];
};

- (id) initWithAttractiveness: (int) attractiveness {
    spriteindex = 0;
    if (self = [super init]) {
        collided = false;
        destroyed = false;
        data = [GameData GetGameData]; // Load in game data (esp. game speed)
        
        CGSize size = [[CCDirector sharedDirector] winSize]; // Window size for position setting
        
        // Initialization of Shrapnel's box2D body definition:
        // - Get the game world from the GameData
        // - Dynamic body allows for object to be affected by world forces and react to collisions
        // – Set to random position off the right side of the screen
        // – Create the body in the world using the bodyDef's properties
        
        b2World *world = [data GetWorld]; // World handles creation/deletion of physics objects
        bodyDef.type = b2_dynamicBody;
        double rand = arc4random()%70;
        int height = ((.15f + (rand/100.0f)) * size.height);
        bodyDef.position.Set((size.width + 3)/PTM_RATIO, height/PTM_RATIO);
        bodyDef.angle = 0; // -(arc4random() * 90);
        body = world->CreateBody(&bodyDef);
        NSLog(@"created");
        // To give a body its size, shape and other characteristics, we add fixtures to it:
        // (Each fixture will affect the mass of the body)
        // - Set shape of fixture as a box oriented on axis at (1, 1)
        // - Set density to 1 (mass = area * density)
        // - Add fixture to the body
        
        bodyShape.SetAsBox(.2f, .2f);
        fixtureDef.shape = &bodyShape;
        fixtureDef.density = 1;
        fixtureDef.userData = self;
        body->CreateFixture(&fixtureDef);
        b2MassData *mass = new b2MassData();
        mass->mass = 0.0000001f;
        body->SetMassData(mass);
        body->SetGravityScale(0.1f);
        body->SetLinearVelocity(b2Vec2(-data.speed,0));
        
        // CCNode *parent = [self getChildByTag:kTagParentNode];
        /*CCSpriteBatchNode *parent = [CCSpriteBatchNode batchNodeWithFile:@"blocks.png" capacity:100];
		CCTexture2D *spriteTexture_ = [parent texture];
        
        //We have a 64x64 sprite sheet with 4 different 32x32 images.  The following code is
        //just randomly picking one of the images
        int idx = (CCRANDOM_0_1() > .5 ? 0:1);
        int idy = (CCRANDOM_0_1() > .5 ? 0:1);
        sprite = [CCPhysicsSprite spriteWithTexture:spriteTexture_ rect:CGRectMake(32 * idx,32 * idy,32,32)];
        sprite = [CCPhysicsSprite spriteWithFile:@"shrapnel-1.png"];
        [sprite setScaleX:sprite.contentSize.width/6];
        [sprite setScaleY:sprite.contentSize.height/6];*/
        //[parent addChild:sprite];
        sprite = [CCPhysicsSprite spriteWithFile:@"shrapnel-0.png"];
        [self setAnimation];
        body->SetAngularVelocity(arc4random_uniform(5)/2.0f);
        body->SetLinearVelocity(b2Vec2(body->GetLinearVelocity().x + (arc4random_uniform(3)/4.0f), (arc4random_uniform(5)/3.0f)));
        [sprite setScaleX:sprite.contentSize.width/6];
        [sprite setScaleY:sprite.contentSize.height/6];
        [sprite setPTMRatio:PTM_RATIO];
        [sprite setB2Body:body];
        [sprite setPosition: ccp( size.width + 3, height)];
    }
    return self;
};
-(void) setAnimation
{
    if(spriteindex <= 5)
    {
        spriteindex++;
        [sprite setTexture:[[CCTextureCache sharedTextureCache] addImage:@"shrapnel-0.png"]];
    }
    else if(spriteindex <=10)
    {
        spriteindex++;
        [sprite setTexture:[[CCTextureCache sharedTextureCache] addImage:@"shrapnel-1.png"]];
    }
    else{spriteindex = 0;}
    //[sprite setTexture:[[CCTextureCache sharedTextureCache] addImage:@"shrapnel-1.png"]];
}
-(id) initAtBottom
{
    self = [self init];
    body->SetTransform(b2Vec2(body->GetPosition().x,3), 0.0f);
    return self;
}

-(void) destroy
{
    if(destroyed){
        return;
    }
    destroyed = true;
    [sprite removeFromParentAndCleanup:YES];
    b2World *world = [data GetWorld];
    world->DestroyBody(body);
}
-(void) touched {
    NSLog(@"shrapnel touched");
    destroyed = true;
    [self destroy];
    // Generate bonus points
}
- (void) update: (ccTime)dt {
    [self setAnimation];
    attractiveness = 5;
    
}
-(CCPhysicsSprite *) GetSprite
{
    return sprite;
}
-(NSString *) GetType
{
    return @"shrapnel";
}
- (void) hitPlayer {
    collided = true;
}
-(bool) hasCollided
{
    return collided;
}
- (CGRect) rect
{
    return CGRectMake((self->sprite.position.x)-(self->sprite.contentSize.width/2), (self->sprite.position.y)-(self->sprite.contentSize.height/2), self->sprite.contentSize.width, self->sprite.contentSize.height);
}

@end
