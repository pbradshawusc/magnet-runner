//
//  SpriteLayer.m
//  Magnet Runner
//
//  Created by Alexa Rucks on 2/16/14.
//  Copyright (c) 2014 ITP382. All rights reserved.
//

#import "SpriteLayer.h"
#import "CCPhysicsSprite.h"
#import "Player.h"
#import "GameData.h"
#import "SimpleAudioEngine.h"
#import "GameplayScene.h"

enum {
	kTagParentNode = 1,
};

@implementation SpriteLayer
-(id) init
{
    if(self = [super init])
    {
        // enable events
		self.touchEnabled = YES;
		self.accelerometerEnabled = YES;
        
        //Load in Game Data
        data = [GameData GetGameData];
		
        Player *p = [Player GetPlayer];
        [self addChild:[p GetSprite]];
		
		
        //End Test Code
    }
    return self;
}

-(void) draw
{
	//
	// IMPORTANT:
	// This is only for debug purposes
	// It is recommend to disable it
	//
	[super draw];
	
	ccGLEnableVertexAttribs( kCCVertexAttribFlag_Position );
	
	kmGLPushMatrix();
	
	[data GetWorld]->DrawDebugData();
	
	kmGLPopMatrix();
}

-(void) update: (ccTime) dt
{
	//It is recommended that a fixed time step is used with Box2D for stability
	//of the simulation, however, we are using a variable time step here.
	//You need to make an informed choice, the following URL is useful
	//http://gafferongames.com/game-physics/fix-your-timestep/
	
	int32 velocityIterations = 8;
	int32 positionIterations = 1;
	
	// Instruct the world to perform a single step of simulation. It is
	// generally best to keep the time step and iterations fixed.
    b2World *world = [data GetWorld];
    if(world == nil)
    {
        return;
    }
	world->Step(dt, velocityIterations, positionIterations);
}

- (void) ccTouchesBegan: (NSSet *)touches withEvent:(UIEvent *)event
{
    for(int i = 0; i < touches.count; i++)
    {
            CGPoint location = [self convertTouchToNodeSpace: [touches.allObjects objectAtIndex:i]];
            
            for (int i = 0; i < [[data GetGameplayScene] getShrapnel].count; i++) {
                Shrapnel *s = [[[data GetGameplayScene] getShrapnel] objectAtIndex:i];
                //NSLog(@"Shrapnel: x: %f, y: %f", [s rect].origin.x, [s rect].origin.y);
                //NSLog(@"Intersection? %s", (CGRectContainsPoint([s rect], location)) ? "true" : "false");
                if (CGRectContainsPoint(CGRectMake([s rect].origin.x, [s rect].origin.y, 40, 40), location)) {
                    //NSLog(@"Touched Shrapnel");
                    //[[[[data GetGameplayScene] getShrapnel] objectAtIndex: i] destroyByClick];
                    [self removeChild:[s GetSprite] cleanup:YES];
                    [[data GetGameplayScene] removeShrapnelAt: i];
                    [s touched];
                    return;
                }
            }
        
            if(dest)
            {
                if(CGRectContainsPoint([dest rect], location))
                {
                    [dest touched];
                    return;
                }
            }
    }
    if([[data GetGameplayScene] GetCompleted])
    {
        return;
    }
    [[data GetGameplayScene] ObjectiveCompleted];
    //Reverse Gravity
    //[[SimpleAudioEngine sharedEngine] playEffect:@"Electricity SE.mp3"];
    b2World *world = [data GetWorld];
    b2Vec2 gravity = world->GetGravity();
    gravity.y = -gravity.y;
    [[Player GetPlayer] stop];
	world->SetGravity(gravity);
}
-(void) setDest:(DestructibleObstacle *)d
{
    dest = d;
}
-(void) checkDest
{
    if([dest getDestroyed])
    {
        [dest destroy];
        NSLog(@"Destroyed dest");
        [[data GetGameplayScene] destroyedWall];
        [[data GetGameplayScene] ObjectiveCompleted];
        dest = nil;
    }
}

- (void)ccTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
	//Add a new body/atlas sprite at the touched location
	for( UITouch *touch in touches ) {
		CGPoint location = [touch locationInView: [touch view]];
		
		location = [[CCDirector sharedDirector] convertToGL: location];
		
		//[self addNewSpriteAtPosition: location];
	}
}

-(void) dealloc
{
	/*delete world;
	world = NULL;
	
	delete m_debugDraw;
	m_debugDraw = NULL;*/
	
	[super dealloc];
}
@end
