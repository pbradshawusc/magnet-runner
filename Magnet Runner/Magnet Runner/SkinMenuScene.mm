//
//  SkinMenuScene.m
//  Magnet Runner
//
//  Created by Edward Foux on 3/6/14.
//  Copyright (c) 2014 ITP382. All rights reserved.
//

#import "SkinMenuScene.h"
#import "cocos2d.h"
#import "MainMenuScene.h"
#import "GameplayScene.h"

@implementation SkinMenuScene

// Helper class method that creates a Scene with the MainMenuLayer as the only child.
+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	SkinMenuScene *layer = [SkinMenuScene node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}

// on "init" you need to initialize your instance
-(id) init
{
    
    
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super's" return value
	if( (self=[super init]) )
	{
		
		// ask director for the window size
		CGSize size = [[CCDirector sharedDirector] winSize];
		
        
        //Add the background image of the magnet runner 
		CCSprite *background;
		background = [CCSprite spriteWithFile:@"background1.png"];
		background.rotation = 90;
		
        if (size.height>size.width){
            background.position = ccp(size.height/2, size.width/2);
        }
        else{
            background.position = ccp( size.width/2,size.height/2);
        }
		
		// add the image as a child to this Layer
		[self addChild: background];
        
        NSString *key = [NSString stringWithFormat:@"playername"];
        
        NSString *val = [[NSUserDefaults standardUserDefaults]
                         
                         stringForKey:key];
        
        //lets find the highest score of current player
         score=[NSNumber numberWithInt:0];
        
        
        score=[self findTheScire:val];

        label = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"Hello %@, your score is %@ ", val,score ] fontName:@"Marker Felt" fontSize:24];
		[self addChild:label z:1];
		[label setColor:ccc3(255,255,255)];
		label.position = ccp( size.width/2, size.height-(label.contentSize.height/2));
		
        // create a menu item using existing sprites
        
        
        CCSprite* normal = [CCSprite spriteWithFile:@"megaman-run-01.png"]; normal.color = ccRED;
        CCSprite* selected = [CCSprite spriteWithFile:@"megaman-run-06.png"]; selected.color = ccGREEN;
        CCMenuItemSprite* item1 = [CCMenuItemSprite
                                   itemWithNormalSprite:normal
                                   selectedSprite:selected
                                   target:self
                                   selector:@selector(transitionBack0)];
        CCSprite* normal2 = [CCSprite spriteWithFile:@"Icon-72.png"]; normal2.color = ccRED;
        CCSprite* selected2 = [CCSprite spriteWithFile:@"Icon.png"]; selected2.color = ccGREEN;
        CCMenuItemSprite* item2 = [CCMenuItemSprite
                                   itemWithNormalSprite:normal2
                                   selectedSprite:selected2
                                   target:self
                                  selector:@selector(transitionBack1)
                                   ];
        CCSprite* normal3 = [CCSprite spriteWithFile:@"Icon-72.png"]; normal3.color = ccRED;
        CCSprite* selected3 = [CCSprite spriteWithFile:@"Icon.png"]; selected3.color = ccGREEN;
        CCMenuItemSprite* item3 = [CCMenuItemSprite
                                   itemWithNormalSprite:normal3
                                   selectedSprite:selected3
                                   target:self
                                   selector:@selector(transitionBack2)
                                   ];
        
    arrayOfIdle=[[NSMutableArray alloc]initWithObjects:normal,normal2,normal3, nil];
        arrayOfSelected=[[NSMutableArray alloc ]initWithObjects:selected,selected2,selected3, nil];
        arrayOfMenuSprites=[[NSMutableArray alloc]initWithObjects:item1,item2,item3, nil];
	
        
        CCMenu* menu = [CCMenu menuWithItems:item1,item2,item3, nil];
        menu.position = CGPointMake(size.width/2, size.height/2);
        [self addChild:menu];
        // aligning is important, so the menu items don't occupy the same location
        [menu alignItemsVerticallyWithPadding:40];
        
        //adjust the labels based on the level
        switch ([score integerValue]) {
            case 0 ... 500:
                [item2 setIsEnabled:NO];
                [item3 setIsEnabled:NO];
               ((CCSprite *)[arrayOfIdle objectAtIndex:0]).color=ccYELLOW;
                ((CCSprite *)[arrayOfIdle objectAtIndex:1]).color=ccBLACK;
                ((CCSprite *)[arrayOfIdle objectAtIndex:2]).color=ccBLACK;
                
                break;
            case 501 ... 1000:
                [item3 setIsEnabled:NO];
                ((CCSprite *)[arrayOfIdle objectAtIndex:0]).color=ccYELLOW;
                ((CCSprite *)[arrayOfIdle objectAtIndex:1]).color=ccYELLOW;
                ((CCSprite *)[arrayOfIdle objectAtIndex:2]).color=ccBLACK;
                break;
                
            default:
                ((CCSprite *)[arrayOfIdle objectAtIndex:0]).color=ccYELLOW;
                ((CCSprite *)[arrayOfIdle objectAtIndex:1]).color=ccYELLOW;
                ((CCSprite *)[arrayOfIdle objectAtIndex:2]).color=ccYELLOW;
                break;
        }
    }
	return self;
    
    
}

-(NSNumber *)findTheScire:(NSString *)nameGiven{
    
    NSNumber * num=[NSNumber numberWithInt:0];
for (int i=0; i<5; i++){
    NSString * key = [NSString stringWithFormat:@"HighScoreIndex%d", i];
    NSString * keyName = [NSString stringWithFormat:@"HighScoreIndexName%d", i];
    NSString *name = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:keyName]];
    if ([name isEqualToString:nameGiven]){
        NSLog(@"have found the name of %@",name);
        num = [NSNumber numberWithInt:[[NSUserDefaults standardUserDefaults] integerForKey:key]];
        break;
    }
}
    return num;
    
}


-(void)transitionBack0{
    NSString *val = [[NSUserDefaults standardUserDefaults]
                     
                     stringForKey:@"playername"];
    NSString * key = [NSString stringWithFormat:@"level"];
    [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInt:0] forKey:key];
    //add the code here to modify the game scene depending upon the level
 [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:0.5 scene:[MainMenuScene node]]];   
}
-(void)transitionBack1{
    NSString *val = [[NSUserDefaults standardUserDefaults]
                     
                     stringForKey:@"playername"];
    NSString * key = [NSString stringWithFormat:@"level"];
    [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInt:1] forKey:key];
    //add the code here to modify the game scene depending upon the level
    if ([[self findTheScire:val] integerValue]>500){
        ((CCSprite *)[arrayOfIdle objectAtIndex:1]).color=ccGREEN;
    [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:0.5 scene:[MainMenuScene node]]]; 
    }
}
-(void)transitionBack2{
    NSString *val = [[NSUserDefaults standardUserDefaults]
                     
                     stringForKey:@"playername"];
    NSString * key = [NSString stringWithFormat:@"level"];
    if ([[self findTheScire:val] integerValue]>1000){
        [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInt:2] forKey:key];
        ((CCSprite *)[arrayOfIdle objectAtIndex:1]).color=ccGREEN;
       [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:0.5 scene:[MainMenuScene node]]];  
    }
    
      
}

@end
