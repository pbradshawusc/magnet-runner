//
//  GameOverScene.m
//  Magnet Runner
//
//  Created by Alexa Rucks on 2/24/14.
//  Copyright (c) 2014 ITP382. All rights reserved.
//

#import "GameOverScene.h"
#import "AppDelegate.h"
#import "MainMenuScene.h"
#import "GameData.h"
#import "SimpleAudioEngine.h"

@implementation GameOverScene
// on "init" you need to initialize your instance
-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super's" return value
	if( (self=[super init]) )
	{
		
		// ask director for the window size
		CGSize size = [[CCDirector sharedDirector] winSize];
		
        
        //Add the background image of the magnet runner
		CCSprite *background;
		background = [CCSprite spriteWithFile:@"background1.png"];
		background.rotation = 90;
		
        if (size.height>size.width){
            background.position = ccp(size.height/2, size.width/2);
        }
        else{
            background.position = ccp( size.width/2,size.height/2);
        }
		
		// add the image as a child to this Layer
		[self addChild: background];
		
		//
		// Leaderboards and Achievements
		//
		
		// Default font size will be 28 points.
		[CCMenuItemFont setFontSize:28];
		
		// Achievement Menu Item using blocks
		CCMenuItem *itemNewGame = [CCMenuItemFont itemWithString:@"Return To Menu" block:^(id sender) {
            [[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
            [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:0.5 scene:[MainMenuScene node]]];
			
		}];
        
        CCMenuItem *itemReplay = [CCMenuItemFont itemWithString:@"Replay" block:^(id sender) {
            
            [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:0.5 scene:[GameplayScene node]]];
			
		}];
        
        
		
		CCMenu *menu = [CCMenu menuWithItems:itemNewGame, itemReplay, nil];
		
		[menu alignItemsVerticallyWithPadding:20];
		//[menu setPosition:ccp( size.height/2, size.width/2 - 125)];
        if (size.height>size.width){
            [menu setPosition:ccp(size.height/2, size.width/4)];
        }
        else{
            [menu setPosition:ccp( size.width/2,size.height/4)];
        }
		
		// Add the menu to the layer
		[self addChild:menu];
        
        //Create a label for the score
        GameData *data = [GameData GetGameData];
        
        NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
        formatter.roundingIncrement = [NSNumber numberWithDouble:0.01];
        formatter.numberStyle = NSNumberFormatterDecimalStyle;
        
        NSString *scoreString = [NSString stringWithFormat:@"Total Score: %@", [formatter stringFromNumber:[NSNumber numberWithInt:data.score]]];
        CCLabelTTF *scoreLabel = [CCLabelTTF labelWithString:scoreString fontName:@"Marker Felt" fontSize:32];
        [scoreLabel setPosition:ccp(size.width/2, 3*size.height/4)];
        [self addChild:scoreLabel];
        
        NSString *distanceString = [NSString stringWithFormat:@"Distance Travelled: %@ m", [formatter stringFromNumber:[NSNumber numberWithFloat:data.distance]]];
        CCLabelTTF *distanceLabel = [CCLabelTTF labelWithString:distanceString fontName:@"Marker Felt" fontSize:26];
        [distanceLabel setPosition:ccp(size.width/2, (3*size.height/4) - scoreLabel.contentSize.height)];
        [self addChild:distanceLabel];
        
        NSString *timeString = [NSString stringWithFormat:@"Time Elapsed: %@ s", [formatter stringFromNumber:[NSNumber numberWithFloat:data.time]]];
        CCLabelTTF *timeLabel = [CCLabelTTF labelWithString:timeString fontName:@"Marker Felt" fontSize:26];
        [timeLabel setPosition:ccp(size.width/2, (distanceLabel.position.y) - distanceLabel.contentSize.height)];
        [self addChild:timeLabel];
        
        [data clean];
	}
	return self;
}

// on "dealloc" you need to release all your retained objects
- (void) dealloc
{
	// in case you have something to dealloc, do it in this method
	// in this particular example nothing needs to be released.
	// cocos2d will automatically release all the children (Label)
	
	// don't forget to call "super dealloc"
	[super dealloc];
}
@end
