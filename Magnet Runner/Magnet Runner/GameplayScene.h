//
//  GameplayScene.h
//  Magnet Runner
//
//  Created by Alexa Rucks on 2/16/14.
//  Copyright (c) 2014 ITP382. All rights reserved.
//

#import "CCScene.h"
#import "GameData.h"
#import "Player.h"
#import "Obstacle.h"
#import "Background.h"

@class SpriteLayer;

@interface GameplayScene : CCScene
{
    SpriteLayer *spriteLayer;
    GameData *gameData;
    Player *player;
    CCLabelTTF * scorelabel;
    CCLabelTTF *label;
    NSMutableArray *obstacles;
    Background *backgroundlayer;
    NSMutableArray *shrapnel;
    
    int obstacleTime;
    int levelNum;
    bool playedSound;
}
-(void) LoseConditionTriggered;
-(void) ObjectiveCompleted;
-(bool) GetCompleted;
-(NSMutableArray *) getShrapnel;
-(void) removeShrapnelAt: (int) i;
-(void) destroyedWall;
@end
